package com.example.pizzeriadonmortadello;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void Cardapio(View view){
        Intent it = new Intent(this,Cardapio.class);
        startActivity(it);
    }
    public void Cadastro(View view){
        Intent it = new Intent(this,Cadastro.class);
        startActivityForResult(it,1);
    }

}
