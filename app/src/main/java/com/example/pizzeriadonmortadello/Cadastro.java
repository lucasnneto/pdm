package com.example.pizzeriadonmortadello;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Cadastro extends AppCompatActivity {

    EditText nome, preco, descricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        nome = findViewById(R.id.p);
        preco = findViewById(R.id.n);
        descricao = findViewById(R.id.descricao);

    }
    public void Salvar(View view){
        Pizza aux = new Pizza();
        aux.setNome(""+nome.getText());
        aux.setPreco(Double.parseDouble(""+preco.getText()));
        aux.setDescricao(""+descricao.getText());
        PizzaDAO.getINSTANCE().setPizzas(aux);
        finish();

    }
}
