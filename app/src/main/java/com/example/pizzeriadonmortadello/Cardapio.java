package com.example.pizzeriadonmortadello;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Cardapio extends AppCompatActivity {
    LinearLayout cardapio;
    ArrayList<Pizza> Pizzas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        cardapio = findViewById(R.id.cardapio);
        Pizzas = new ArrayList<>();
        Pizzas = PizzaDAO.getINSTANCE().getPizzas();

        //RECEBER O VALOR
        for (Pizza p : Pizzas) {
            View v = View.inflate(this, R.layout.pedaco, null);
            TextView preco = v.findViewById(R.id.p);
            final TextView ingre = v.findViewById(R.id.ingre);
            Button nome = v.findViewById(R.id.n);
            nome.setText(p.getNome());
            ingre.setText(p.getDescricao());
            preco.setText(""+p.getPreco());
            nome.setOnClickListener(new  View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int ver =  ingre.getVisibility()== View.GONE ? View.VISIBLE : View.GONE;
                    ingre.setVisibility(ver);
                }
            });
            cardapio.addView(v);
        }
    }
}
