package com.example.pizzeriadonmortadello;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Pizza implements Comparable<Pizza> {
    private String nome, descricao;
    private double preco;

    public Pizza() {
    }

    public Pizza(String nome, String descricao, double preco) {
        this.nome = nome;
        this.descricao = descricao;
        this.preco = preco;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", preco=" + preco +
                '}';
    }

    @Override
    public int compareTo(Pizza o) {
        return this.getNome().toUpperCase().compareTo(o.getNome().toUpperCase());
    }
}
