package com.example.pizzeriadonmortadello;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class PizzaDAO {
    private  static PizzaDAO INSTANCE;
    private ArrayList<Pizza> pizzas;

    public PizzaDAO() {
        this.pizzas = new ArrayList<>();

    }

    public static  final PizzaDAO getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE = new PizzaDAO();
        }
        return  INSTANCE;
    }


    public ArrayList<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(Pizza pizza) {
        this.pizzas.add(pizza);
        Collections.sort(this.pizzas);
    }
}
